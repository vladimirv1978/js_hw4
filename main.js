let firstNumber = prompt("Enter your first number", "");
let operatorMath = prompt("Enter a mathematical operator", "");
let secondNumber = prompt("Enter your second number", "");

while ( isNaN(+firstNumber) || isNaN(+secondNumber) || firstNumber === null || secondNumber === null || firstNumber == "" || secondNumber == "") {
  if (firstNumber === null)  firstNumber = '';       //чтобы после нажатия "Отмена" в поле не отображался "null"
  if (secondNumber === null)  secondNumber = '';     //чтобы после нажатия "Отмена" в поле не отображался "null"
  firstNumber = prompt("One or two operands entered incorrectly. Enter your first number", firstNumber);
  secondNumber = prompt("Enter your second number", secondNumber);
}

calculations(firstNumber, operatorMath, secondNumber);

function calculations(operand1, operator, operand2) {
  switch (operator) {
    case "/":
        if (operand2 == 0) {
            console.log("Can't divide by zero");
            break
        }
      console.log(`result ${operand1} / ${operand2} = ${operand1 / operand2}`);
      break;
    case "*":
      console.log(`result ${operand1} * ${operand2} = ${operand1 * operand2}`);
      break;
    case "+":
      console.log(
        `result ${operand1} + ${operand2} = ${+operand1 + +operand2}`
      );
      break;
    case "-":
      console.log(`result ${operand1} - ${operand2} = ${operand1 - operand2}`);
      break;
    default:
      console.log("Operator entered incorrectly");
  }
}
